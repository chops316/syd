var keystone = require('keystone');
var Types = keystone.Field.Types;

var Coach = new keystone.List('Coach', {
  track: true,
  autokey: {path: 'key', from: 'User', unique: true}
});

Coach.add({
  name: { type: String, required: true, index: true },
  user: { type: Types.Relationship, ref: 'User', required: true, initial: true},
  clients: { type: Types.Relationship, ref: 'User', many:true},
  organisation: {type: Types.Relationship, ref: 'Organisation', many:false},
  isCoach: { type: Boolean, default: true }
});

Coach.relationship({ ref: 'User', refPath: 'coach', path: 'users' });
Coach.relationship({ ref: 'Organisation', refPath: 'coaches', path: 'organisations'})

Coach.defaultColumns = "User, Organisation";
Coach.register();
