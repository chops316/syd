var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Habit Comments Model
 * ===================
 */

var Comment = new keystone.List('Comment', {
    nocreate: true
});

Comment.add({
    author: { type: Types.Relationship, ref: 'User', index: true },
    date: { type: Types.Date, default: Date.now, index: true },
    content: { type: Types.Markdown }
});

Comment.relationship({ ref: 'Activity', refPath: 'activity', path: 'comments' });




/**
 * Registration
 * ============
 */

Comment.defaultColumns = 'author, date|20%';
Comment.register();
/**
 * Created by fitne on 2/1/2016.
 */
