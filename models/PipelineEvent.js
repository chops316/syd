var async = require('async');
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PipelineEvents Model
 * ===========
 */

var PipelineEvent = new keystone.List('PipelineEvent', {
    map: { name: 'title' },
    track: true,
    autokey: { path: 'slug', from: 'title', unique: true }
});

PipelineEvent.add({
    
    description: {type: Types.Markdown, index: true},
    //need to get the object id from the coach
	
    creator: {type: Types.Relationship, ref: 'Coach', index: true},
    //publishedDate: { type: Types.Date, index: true }
   
});

/**
 * Virtuals
 * ========
 */
PipelineEvent.schema.virtual('content.full').get(function() {
    return this.content.extended || this.content.brief;
});


/**
 * Relationships
 * =============
 */





/**
 * Notifications
 * =============
 */

PipelineEvent.schema.methods.notifyAdmins = function(callback) {
    var pipelineEvent = this;
    // Method to send the notification email after data has been loaded
    var sendEmail = function(err, results) {
        if (err) return callback(err);
        async.each(results.admins, function(admin, done) {
            new keystone.Email('admin-notification-new-pipelineEvent').send({
                admin: admin.name.first || admin.name.full,
                author: results.author ? results.author.name.full : 'Somebody',
                title: pipelineEvent.title,
                keystoneURL: 'http://www.sydjs.com/keystone/pipelineEvent/' + pipelineEvent.id,
                subject: 'New PipelineEvent on Fitpath'
            }, {
                to: admin,
                from: {
                    name: 'FitPath',
                    email: 'info@thomlamb.com'
                }
            }, done);
        }, callback);
    }
    // Query data in parallel
    async.parallel({
        author: function(next) {
            if (!pipelineEvent.author) return next();
            keystone.list('User').model.findById(pipelineEvent.author).exec(next);
        },
        admins: function(next) {
            keystone.list('User').model.find().where('isAdmin', true).exec(next)
        }
    }, sendEmail);
};


/**
 * Registration
 * ============
 */

PipelineEvent.defaultSort = '-createdAt';
PipelineEvent.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
PipelineEvent.register();

/**
 * Created by fitne on 2/2/2016.
 */

var Lead = new keystone.List('Lead', { inherits: PipelineEvent});
Lead.add({ isLead: { type: Boolean, default: true } });


Lead.relationship({ ref: 'User', refPath: 'lead',many: true,  path: 'leads' });
Lead.register();

var Tour = new keystone.List('Tour', { inherits: PipelineEvent});
Tour.add({ isTour: { type: Boolean, default: true },
	appointmentTime: {type: Types.Date }
		

 });


Tour.relationship({ ref: 'User', refPath: 'tour',many: true,  path: 'tours' });
Tour.register();

var Assessment = new keystone.List('Assessment', { inherits: PipelineEvent});
Assessment.add({ isAssessment: { type: Boolean, default: true } });


Assessment.relationship({ ref: 'User', refPath: 'assessments',many: true,  path: 'assesssments' });
Assessment.register();
var Sale = new keystone.List('Sale', { inherits: PipelineEvent});
Sale.add({ isSale: { type: Boolean, default: true } });


Sale.relationship({ ref: 'User', refPath: 'sale',many: true,  path: 'sales' });
Sale.register();