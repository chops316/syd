var keystone = require('keystone');
var Types = keystone.Field.Types;

var Response = new keystone.List('Response', {
	track: true
});

Response.add({
	content: {type: String, required: true, initial: true}
});

Response.defaultColumns = "content";
Response.register();

