var keystone = require('keystone');
var Types = keystone.Field.Types;

var TaDa = new keystone.List('TaDa', {
   track: true,
   autokey: {path: 'key', from: 'title', unique: true} 
});

var deps = {
    
    habit: {'task': 'Habit'},
    workout: {'task': 'Workout'},
    survey: {'task': 'Survey'},
    reminder: {'reminder.remind': true}
   
}

TaDa.add({
        title: { type: Types.Text, required: true, index:true, initial:true },
        client: { type: Types.Relationship, ref: 'User', initial:true, required: true},
        coach: { type: Types.Relationship, ref: 'Coach', initial:true, required: true}
    },{
       task: { type: Types.Select , options: [
            'Habit',
            'Workout',
            'Survey'
        ]} 
    },
    {
         habit: {
            start: { type: Types.Datetime, default: Date.now, dependsOn: deps.habit},
            end: { type: Types.Datetime, dependsOn: deps.habit},
            reoccurance: {type: Types.Text, dependsOn: deps.habit}
         }
    },
    {
        workout: {
            type: { type: Types.Select , options: [
                'Upper Body',
                'Lower Body',
                'Cardio',
                'Home',
                'Rehab',
                'Other'
            ], dependsOn: deps.workout}
        }
    },
    {
        survey: {
            title: { type: Types.Text, dependsOn: deps.survey},
            description: { type: Types.Markdown, dependsOn: deps.survey}
        }
    },
    {
         reminder: {
            remind: { type: Boolean, label: 'Send Reminders?'}, 
            start: { type: Types.Datetime, default: Date.now, dependsOn: deps.reminder},
            end: { type: Types.Datetime, dependsOn: deps.reminder},
            reoccurance: {type: Types.Text, dependsOn: deps.reminder}
         }
    },
    'Response',
    {
        response: {
            description: { type: Types.Markdown}
        }
    }
   
);

TaDa.defaultColumns = 'title, client, task';
TaDa.register();

