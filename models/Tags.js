var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Link Tags Model
 * ===============
 */

var Tag = new keystone.List('Tag', {
    autokey: { from: 'name', path: 'key', unique: true }
});

Tag.add({
    name: { type: String, required: true }
});


/**
 * Relationships
 * =============
 */

Tag.relationship({ ref: 'Link', refPath: 'tags', path: 'links' });
Tag.relationship({ ref: 'Habit', refPath: 'tags', path: 'habits' });
Tag.relationship({ ref: 'Workouts', refPath: 'tags', path: 'workouts' });
Tag.relationship({ ref: 'Check-ins', refPath: 'tags', path: 'Check-ins' });




/**
 * Registration
 * ============
 */

Tag.register();
/**
 * Created by fitne on 2/2/2016.
 */
