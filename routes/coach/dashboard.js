var keystone = require('keystone');
var async = require('async');
var Coach = keystone.list('Coach');
var User = keystone.list('User');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req,res);
	var locals = res.locals;
	
	locals.section = 'dashboard';
	locals.filter = {
		coach: req.user.coach
	};

	locals.current = req.user;
	
	// Load Coach's clients
	view.on('init', function (next) {
		if(req.user.coach) {
			Coach.model.findOne({_id: locals.filter.coach}).exec(function(err, result) {
			locals.clients = result.clients;
			console.log(locals.clients);
			next(err);
			});
		} else {
			next();
		}
	});
	
	// Note this might have shitty performance?

	// Locate and populate client information into tables
	view.on('init', function (next) {
		if (locals.clients) {
			User.model.find({
				_id: { $in: locals.clients },
			}).exec(function (err, result) {
				locals.details = result;
				console.log(locals.details);
				next(err);	
			});			
		} else {
			next();
		}
	});
	
	view.render('coach/dashboard');
}